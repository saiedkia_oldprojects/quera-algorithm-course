#include <iostream>

using namespace std;
int main()
{
    long n;
    cin >> n;
    long sum = 0;
    while (n >= 10)
    {
        sum = 0;
        while (n > 0)
        {
            sum = sum + (n % 10);
            n = n / 10;
        }
        n = sum;
    }

    cout << n;

    return 0;
}
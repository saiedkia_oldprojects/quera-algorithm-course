#include <iostream>

using namespace std;
inline int in() { int x; scanf("%d", &x); return x; }
const int N = 2002;

int charCount(string s)
{
	int cnt[26] = { }, nPos = 0;
	for(int i = 0; i < s.size(); i++)
	{
		if(!cnt[s[i] - 'a'])
			nPos++;
		cnt[s[i] - 'a']++;
	}
	return nPos;
}

int main()
{
	int n = in(), ans = 0;
	for(int i = 0; i < n; i++)
	{
		string s;
		cin >> s;
		ans = max(ans, charCount(s));
	}
	cout << ans << endl;
}

    // https://raw.githubusercontent.com/IOTahaCodes/QueraCollegeSolutions/master/Chap06/Q06.cpp
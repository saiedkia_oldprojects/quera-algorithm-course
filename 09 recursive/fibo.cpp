#include <iostream>

using namespace std;
int main()
{
    int n;
    cin >> n;
    int index = 1;
    int prevFibo = 1;
    int lastFibo = 1;
    while (index <= n)
    {
        if (index == lastFibo)
        {
            cout << "+";
            int tmp = lastFibo;
            lastFibo += prevFibo;
            prevFibo = tmp;
        }
        else
        {
            cout << "-";
        }
        index++;
    }
}
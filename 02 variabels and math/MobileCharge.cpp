#include <iostream>

int main()
{
    int result = 0;

    char currentChar[20];
    std::cin >> currentChar;
    int value = 0;
    int multiply = 1;

    for (int i = 0; i < 20; i++)
    {
        if (currentChar[i] == '\0')
        {
            break;
        }

        value = (value * multiply) + (int)(currentChar[i] - '0');
        multiply *= 10;
    }

    if (value > 100 || value < 0)
    {
        std::cout << 0;
    }
    else
    {
        result = value * (value + 1) / 2;
        std::cout << result;
    }

    return 0;
}

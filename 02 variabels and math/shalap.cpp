#include <iostream>
#include <iomanip>
#include <math.h>

using namespace std;
int main(){
    long long n;
    double out;
    cin >> n;

    if(n & 1) {
        out = (n - 1) / double(4);
    } else {
        out = pow(n, 2) / double(4 + (4 * n));
    }

    cout << fixed << setprecision(6) << out;
}



// https://github.com/IOTahaCodes/QueraCollegeSolutions/blob/master/Chap03/Q06.cpp
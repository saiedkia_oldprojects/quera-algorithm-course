#include <iostream>


using namespace std;
int counter[100];

int main() {
  int n;
  cin >> n;
  for(int i = 0; i < n; i++) {
    int x;
    cin >> x;
    counter[x]++;
  }
  int mn = -1;
  for(int i = 1; i <= 100; i++) {
    if(!counter[i])
      continue;
    if(mn == -1){
      mn = i;
      continue;
    }
    if(counter[mn] > counter[i])
      mn = i;
  }
  cout << mn << '\n';
}
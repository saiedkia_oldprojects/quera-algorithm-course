// In the name of God

#include<iostream>
#include<iomanip>
using namespace std;

double rad = 1.732050808; // this equels radical(3)

void go(int n,double X1, double Y1, double X2,int sign,int my){
	// (X1, Y1) and (X2, Y2) are points on the base, we have Y1 = Y2
	// if sign = 1 the other point is higher
	// sign = -1 the other point is lower
	// if my = 1 then we should only output (X1, Y1)
	// if my = 2 then we should only output (X2, Y2)
	// if my = 3 then we should only output (X3, Y3)
	double X3 = (X1 + X2) / 2 , Y3 = Y1 + sign * rad / 2 * (X2 - X1) , Y2 = Y1;
	// calculating (X3 , Y3)	
	if(n == 0){
		if(my == 1) cout << setprecision(6) << fixed << X1 << ' ' << Y1 << '\n';
		if(my == 2) cout << setprecision(6) << fixed << X2 << ' ' << Y2 << '\n';
		if(my == 3) cout << setprecision(6) << fixed << X3 << ' ' << Y3 << '\n';		
		return;
	}
	if(n == 1) my = 1;
	go(n-1 , X1 , Y1 + 2.0 / 3 * (Y3 - Y1) , X1 + 2.0 / 3 * (X3 - X1)  , -sign , my);
	if(n == 1) my = 2;
	go(n-1 , X2 - 2.0 / 3 * (X2 - X3) , Y2 + 2.0 / 3 * (Y3 - Y2) , X2 , -sign , my);
   	if(n == 1) my = 3;	
	go(n-1 , X1 + 1.0 / 3 * (X2 - X1) , Y1 , X1 + 2.0/3 * (X2 - X1) , -sign , my);
}

int main(){
	int n;
	cin >> n;
	go(n , 0 , 0 , 1000 , 1 , 0);
	return 0;	
}


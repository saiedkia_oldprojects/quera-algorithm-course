// In the name of God

#include<iostream>
using namespace std;

int min(int a,int b){
	return a < b ? a : b;
}

void swap(int &a,int &b){
	int tmp = a;
	a = b;
	b = tmp;
}

void sort(int *L,int *R){
	// We are going to sort a subsequence
	// L is the pointer to the begining and
	// R is the iterator after the end of the subsequence
	int len = R - L;
	// len is the length of the subsequence
	for(int repeat=0;repeat<len;repeat++)
		for(int i=0;i<len-1;i++)
			if(*(L+i) > *(L+i+1))
				swap(*(L+i), *(L+i+1));
}

int n , k;
int a[5050];

int main(){
	cin >> n >> k;
	for(int i=0;i<n;i++)
		cin >> a[i];
	sort(a , a + n);
	int answer = 1000 * 1000 * 1000; // a very large number
	for(int i=0;i<n;i++){
		int counter = 1;
		int p = i+1;
		while(p < n){
			if(a[p] != a[p-1])
				counter++;
			if(counter == k)
				answer = min(answer, a[p] - a[i]);
			p++;
		}
	}
	if(answer >= 1000 * 1000 * 1000)
		cout << "-1\n";
	else
		cout << answer << '\n';
	return 0;	
}

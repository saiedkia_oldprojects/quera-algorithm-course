#include <iostream>

int revNumber(char *nums);
using namespace std;
int main()
{
    char *one = new char[3];
    char *two = new char[3];

    cin >> one;
    cin >> two;

    if (revNumber(one) < revNumber(two))
    {
        char *tmp = one;
        one = two;
        two = tmp;
    }

    if (two[2] == one[2] && two[1] == one[1] && two[0] > one[0])
        cout << two << " > " << one;
    else if (two[2] == one[2] && two[1] > one[1])
        cout << two << " > " << one;
    else if (two[2] > one[2])
        cout << two << " > " << one;
    else if (two[2] == one[2] && two[1] == one[1] && two[0] < one[0])
        cout << two << " < " << one;
    else if (two[2] == one[2] && two[1] < one[1])
        cout << two << " < " << one;
    else if (two[2] < one[2])
        cout << two << " < " << one;
    else
        cout << two << " = " << one;

    return 0;
}

int revNumber(char *nums)
{
    int number = 0;
    int multi = 1;
    for (int i = 0; i < 3 && nums[i] != '\0'; i++)
    {
        number += (nums[i] - '0') * multi;
        multi *= 10;
    }

    return number;
}

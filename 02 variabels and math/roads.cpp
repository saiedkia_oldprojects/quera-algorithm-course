#include <iostream>

using namespace std;
int main()
{
    int num;
    cin >> num;

    if (num < 1 || num > 100)
    {
        cout << "0";
    }
    else
    {
        int max = 1;
        if (num == 1)
            max = 2;
        else if (num == 0)
            max = 0;
        else
        {
            int x = num / 2;
            int y = num - x;

            max = (x + 1) * (y + 1);
        }

        cout << max;
    }


    return 0;
}
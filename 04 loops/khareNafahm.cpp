#include <iostream>

using namespace std;
int main()
{
    int x, y;
    cin >> x >> y;

    int currentX = 0, currentY = 0;
    int upDown = true;
    int value = -1;

    int max = x + y + 50;
    for (int i = 0; i < max; i++)
    {
        //cout << currentX << " " << currentY << endl;
        if (x == currentX && y == currentY)
        {
            value = i;
            break;
        }
        else
        {
            if (i % 2 != 0)
            {
                if (upDown)
                {
                    currentX++;
                    currentY--;
                }
                else
                {
                    currentX--;
                    currentY++;
                }
                upDown = !upDown;
            }
            else
            {
                currentX++;
                currentY++;
            }
        }
    }

    cout << value;
    return 0;
}
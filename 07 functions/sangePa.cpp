#include <iostream>

using namespace std;

bool is_prime(int num)
{
    if (num <= 1)
        return false;
        
    bool isPrime = true;
    for (int i = 2; i < num; i++)
        if (num % i == 0)
            isPrime = false;

    return isPrime;
}

int get_prime_count(int num)
{
    int count = 0;
    for (int i = 1; i < num; i++)
        if (is_prime(i))
        {
            //cout << i << endl;
            count++;
        }
    return count;
}

int get_dividable_count(int num)
{
    int count = 0;
    for (int i = 1; i < num; i++)
        if (is_prime(i) && num % i == 0)
            count++;
    return count;
}

int main()
{
    int n = 0;
    cin >> n;

    int totalPrice = 0;
    int index = 0;
    while (index < n)
    {
        int num;
        cin >> num;
        index++;

        if (num <= 1 || num > 1001)
            continue;

        int primeCount = get_prime_count(num);
        int dividableCount = get_dividable_count(num);
        if (is_prime(num))
            totalPrice += primeCount;
        else
            totalPrice += dividableCount;
    }

    int primeCount = get_prime_count(totalPrice);
    int dividableCount = get_dividable_count(totalPrice);

    if (is_prime(totalPrice))
        totalPrice -= primeCount;
    else
        totalPrice -= dividableCount;

    cout << totalPrice;
    return 0;
}
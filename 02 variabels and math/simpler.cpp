#include <iostream>
#include <iomanip>

using namespace std;
void findMinMax(double nums[], double *minMax, int count);
int main(){
    double a, b, c, d;
    cin >> a;
    cin >> b;
    cin >> c;
    cin >> d;

    double nums[] = {a, b, c, d};
    double *minMax = new double[2];
    findMinMax(nums, minMax, 4);
    cout << "Sum : " << fixed << setprecision(6) << (a + b + c + d) << endl;
    cout << "Average  : " << fixed << setprecision(6) << (a + b + c + d) / 4 << endl;
    cout << "Product  : " << fixed << setprecision(6) << (a * b * c * d) << endl;
    cout << "MAX : " << fixed << setprecision(6) << *minMax << endl;
    cout << "MIN : " << fixed << setprecision(6) << *(minMax + 1) << endl;
    return 0;
}


void findMinMax(double nums[], double *minMax, int count){
    minMax[0] = nums[0];
    minMax[1] = nums[0];

    for(int i = 0; i < count; i++){
        if(minMax[0] < nums[i] ) { minMax[0] = nums[i];}
        if(minMax[1] > nums[i] ) { minMax[1] = nums[i];}
    }
}
// In the name of God

#include<iostream>
using namespace std;

long long c[110][110];
bool seen[110][110];

long long C(int a,int b){ // aRb
	if(seen[a][b])
		return c[a][b];
	seen[a][b] = true;
	if(b == 0 || a == b){
		c[a][b] = 1;
		return c[a][b];
	}
	c[a][b] = C(a-1, b) + C(a-1, b-1);
	return c[a][b];
}

int main(){
	int n;
	cin >> n;
	for(int i=0;i<n;i++){
		for(int j=0;j<=i;j++)
			cout << C(i, j) << ' ';
		cout << '\n';
	}
	return 0;
}

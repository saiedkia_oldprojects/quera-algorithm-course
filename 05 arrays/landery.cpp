#include <iostream>

using namespace std;

int main()
{
  char **results = (char**)malloc(sizeof(char) * 10);
  int outIndex = 0;
  int *technisian[100];
  int dryness[100];

  int n, m;
  cin >> n >> m;

  while (true)
  {
    char *s = new char[500];
    cin >> s;
    if (s[0] == 'p')
      break;

    int index = 0;
    for (int i = 0; s[i] != '\0'; i++)
      index = 10 * index + (s[i] - '0');

    cin >> s;
    if (s[0] == 't')
    {
      int B;
      cin >> B;
      technisian[index] = &dryness[B];
    }
    else
    {
      bool not_assigned = false;
      if (technisian[index] == NULL)
        not_assigned = true;

      if (s[0] == 'd')
      {

        int d;
        cin >> d;
        if (not_assigned)
        {
          cout << "no towel has been assigned to me.\n";
          results[outIndex] = "no towel has been assigned to me.\n";outIndex++;
          continue;
        }
        (*technisian[index]) -= d;
        if ((*technisian[index]) < 0)
          *technisian[index] = 0;
      }
      else if (s[0] == 'g')
      {

        if (not_assigned)
        {
          cout << "no towel has been assigned to me.\n";
          results[outIndex] = "no towel has been assigned to me.\n";outIndex++;
          continue;
        }
        if ((*technisian[index]) == 0)
        {
          cout << "ok\n";
          results[outIndex] = "ok\n";outIndex++;
          (*technisian[index]) = 10;
        }
        else{
          cout << "impossible\n";
          results[outIndex] = "impossible\n";outIndex++;
        }
      }
      else
      {
        if (not_assigned)
        {
          cout << "no towel has been assigned to me.\n";
          results[outIndex] = "no towel has been assigned to me.\n";outIndex++;
          continue;
        }
        cout << *technisian[index] << '\n';
        //results[outIndex] = (char)(*technisian[index]) ;outIndex++;
      }
    }
  }

  for (int i = 0; i < 10; i++){
    if (results[i])
      cout << results[i];
  }
  return 0;
}
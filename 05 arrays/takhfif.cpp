#include <iostream>

bool validateCode(char *code, char *validCode);
using namespace std;
int main()
{
    int n;
    char *validCode = (char *)malloc(sizeof(char) * 1024);
    cin >> n >> validCode;

    for (int i = 0; i < n; i++)
    {
        char *code = new char[1024];
        cin >> code;
        if (validateCode(code, validCode))
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }
}

bool validateCode(char *code, char *validCode)
{
    int i = 0, j = 0;
    bool isValidOne = true;
    bool isValidTwo = true;
    while (code[i] != '\0')
    {
        j = 0;
        bool contains = false;
        while (validCode[j] != '\0')
        {
            if (validCode[j] == code[i])
            {
                contains = true;
                break;
            }

            j++;
        }

        if (!contains)
        {
            isValidOne = false;
            break;
        }

        i++;
    }

    i = j = 0;
    while (validCode[i] != '\0')
    {
        j = 0;
        bool contains = false;
        while (code[j] != '\0')
        {
            if (validCode[i] == code[j])
            {
                contains = true;
                break;
            }

            j++;
        }

        if (!contains)
        {
            isValidTwo = false;
            break;
        }

        i++;
    }

    return isValidOne && isValidTwo ;
}
#include <iostream>

using namespace std;
int main()
{
    int row;
    cin >> row;
    int rows[1204];
    int sum = 0;
    for (int i = 0; i < row; i++)
    {
        int value = 0;
        cin >> value;
        rows[i] = value;
        sum += value;
    }

    int max = sum / row;
    sum = 0;
    for (int i = 0; i < row; i++)
    {
        if (rows[i] < max)
            sum += max - rows[i];
    }

    cout << sum;
    return 0;
}
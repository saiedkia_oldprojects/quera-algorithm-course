// In the name of God

#include<iostream>
using namespace std;

int counter = 1;

void goHanoi(int n,char from,char to,char help){
	if(!n) return;
	goHanoi(n-1, from, help, to);
	cout << counter << ' ' << from << ' ' << to << '\n';
	counter++;
	goHanoi(n-1, help, to, from);	
}

int main(){
	int n;
	cin >> n;
	goHanoi(n, 'A', 'B', 'C');
	return 0;
}
// paid by my scores... :(

// In the name of God

#include<bits/stdc++.h>
using namespace std;
#define pb push_back
#define Size(x) ((int)(x).size())
typedef long long ll;
typedef long double ld;
typedef pair<int ,int>pii;
const int INF = 1e9 + 10;

int lcm(int a,int b){
	return a * b / __gcd(a , b);
}

int main(){
	ios::sync_with_stdio(false) , cin.tie(0) , cout.tie(0);
	int n;
	cin >> n;
	int ans = 1;
	for(int i=1;i<n;i++)
		if(__gcd(i , n) == 1)
			ans = lcm(ans , i);	
	cout << ans << '\n';
	return 0;
}


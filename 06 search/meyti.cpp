#include <iostream>
#include <algorithm>

using namespace std;
int main()
{
    int length;
    cin >> length;

    unsigned long answer = 0;
    int rep = length / 3;
    for(int i = 1; i <= rep; i++)
    {
        long long v = ((answer +  ((length - (3 * i))/ 2)) - max(0, ((length / 2) - (2 * i)) + 1)) + 1;
        answer = v;
    }

    
    cout << answer;
    return 0;
}
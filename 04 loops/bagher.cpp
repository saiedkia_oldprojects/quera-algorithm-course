#include <iostream>

using namespace std;
int main()
{
  int n, d, x = 0, t = 0;
  cin >> n >> d;
  
  for(int i = 0; i < n; i++)
  {
    int l, c, z;
    cin >> l >> c >> z;
    t += l - x;
    x = l;
    while(t % (c + z) < c) ++t;
  }
  
  t += d - x;
  x = d;
  cout << t;

  return 0;
}



// https://github.com/IOTahaCodes/QueraCollegeSolutions/blob/master/Chap05/Q05.cpp
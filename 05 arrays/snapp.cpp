#include <iostream>

using namespace std;
int main()
{
    int n, m, k = 0;
    cin >> n >> m;
    int data[100][100];

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            cin >> data[i][j];
        }
    }

    int total = 0;
    for (int x = 0; x < m; x++)
    {
        int x1, y1;
        cin >> x1 >> y1;
        total += data[x1 - 1][y1 - 1];
    }

    cout << total;

    return 0;
}
#include <iostream>

using namespace std;

void sort(int *arr, int index)
{
    int temp;
    for (int i = 0; i < index; i++)
    {
        for (int j = i + 1; j < index; j++)
        {
            if (arr[j] < arr[i])
            {
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
}

void insertion_sort(int arr[], int length)
{
    int i, j, tmp;
    for (i = 1; i < length; i++)
    {
        j = i;
        while (j > 0 && arr[j - 1] > arr[j])
        {
            tmp = arr[j];
            arr[j] = arr[j - 1];
            arr[j - 1] = tmp;
            j--;
        } //end of while loop

    } //end of for loop
} //end of insertion_sort.

int main()
{
    int count, i = 0, index = 0;
    int items[5001];
    cin >> count;

    while (i < count)
    {
        char *command = new char[3];
        int value;
        cin >> command >> value;

        if (command[2] == 'k')
        {
            //sort(items, index);
            insertion_sort(items, index);
            cout << items[value - 1] << endl;
        }
        else
        {
            items[index] = value;
            index++;
        }

        i++;
    }
}
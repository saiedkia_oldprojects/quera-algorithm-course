#include <iostream>

using namespace std;
int main()
{

    int a, b;
    cin >> a >> b;

    // if (a < 0 || a > 12)
    // {
    //     cout << "00";
    //     return 0;
    // }

    // if (b < 0 || b > 59)
    // {
    //     cout << 0;
    //     return 0;
    // }

    a = 12 - a;
    b = 60 - b;

    if (a >= 12 || a < 0)
        a = 0;

    if (b >= 60 || b < 0)
        b = 0;

    if (a < 10)
        cout << "0" << a << ":";
    else
        cout << a << ":";

    if (b < 10)
        cout << "0" << b;
    else
        cout << b;

    return 0;
}
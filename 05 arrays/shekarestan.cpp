#include <iostream>

using namespace std;
int main()
{
    int row, column, count = 0;
    cin >> row >> column;

    for (int i = 0; i < row; i++)
    {
        char sky[column];
        cin >> sky;
        for (int j = 0; j < column; j++)
            if (sky[j] == '*')
                count++;
    }

    cout << count;
    return 0;
}